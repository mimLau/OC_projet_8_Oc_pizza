# Documentation du système de gestion d'une pizzeria

## Table of contents 
* [Informations générales](#général) 
* [Diagrammes du dossier de conception fonctionnelle](#fonctionelle) 
* [Diagrammes dossier de conception technique](#technique) 


## Informations générales 

### Contexte

« OC Pizza » est un jeune groupe de pizzeria en plein essor et spécialisé dans les pizzas livrées ou à emporter. Il compte déjà 5 points de vente et prévoit d’en ouvrir au moins 3 de plus d’ici la fin de l’année. Un des responsable du groupe a pris contact avec vous afin de mettre en place un système informatique sur-mesures, déployé dans toutes ses pizzerias.

### Besoins 

Ce système informatique permettrait :

* d’être plus efficace dans la gestion des commandes, de leur réception à leur livraison en passant par leur préparation
* de suivre en temps réel les commandes passées et en préparation
* de suivre en temps réel le stock d’ingrédients restants pour savoir quelles pizzas sont encore réalisables
* de proposer un site internet pour que les clients puissent :
** passer leurs commandes, en plus de la prise de commande par téléphone ou sur place
** payer en ligne leur commande s’il le souhaite, sinon, ils paieront directement à la livraison
** modifier ou annuler leur commande tant que celle-ci n’a pas été préparée
* de proposer un aide mémoire aux pizzaiolos indiquant la recette de chaque pizza
* d’informer ou notifier les clients sur l’état de leur commande

### Travail demandé

Rédiger les documents suivants :

* Dossier de conception fonctionnelle : à l’attention de la maîtrise d’ouvrage (MOA) et de la maîtrise d’œuvre (MOE)
* Dossier de conception technique : à l’attention des développeurs, mainteneurs et de l’équipe technique du client 
* Dossier d’exploitation : à l’attention de l’équipe technique du client
* Procès-verbal de livraison finale



## Diagrammes du dossier de conception fonctionnelle

### Diagramme de contexte

![Diagramme contexte](Images/Oc_pizza_Diagramme_de_contexte.PNG "Diagramme de contexte")

### Diagramme de package

![Diagramme package](Images/Oc_pizza_Diagrammes_de_package.png "Diagramme de package")

### Diagramme de classe

![Diagramme classe](Images/Oc_pizza_Diagramme_de_classe.png "Diagramme de classe")

### Diagramme d'activités

#### - Commande en ligne

![Diagramme activité](Images/Diagramme d'activités-Commande en ligne.png "Commande en ligne")

#### - Préparation de commande

![Diagramme activité](Images/Diagramme d'activités-Préparation de commande.png "Préparation commande")

### Diagramme d'états : cycle de vie d'une commande

![Diagramme d'états](Images/Diagramme d'états transitions.png "Cycle de vie d'une commande")


## Diagrammes du dossier de conception technique

### Diagramme de composants

![Diagramme de composants](Images/Oc_pizza_Diagramme_composants.png "Diagramme de composants")

### Diagramme de composants

![Diagramme de composants](Images/Oc_pizza_diagramme deploiement.png "Diagramme de composants")

### Modèle physique de données

![MDP](Images/OC_pizza_model_physique_donnees.png "MDP")
